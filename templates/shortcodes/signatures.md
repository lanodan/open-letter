{% set sigs = load_data(path="signatures.csv") -%}
{% for sig in sigs.records|sort(attribute="0") %}
- {% if sig[2] %}<a href="{{sig[2]}}">{{sig[0]}}</a>{% else %}{{sig[0]}}{% endif %} ({{sig[1]}})
{%- endfor -%}