# An open letter from developers to the wider Linux community.

We are a group of developers in the Linux ecosystem. 
We share the common goal of producing software that offers a great, stable, and consistent user experience.
However, this is harmed when software is shipped too early to users.

Therefore, **we kindly ask to only ship tagged releases to users 
or consult the developer first who wrote the patches before shipping them**.

Want to know more? Read the [full letter here](https://do-not-ship.it/)

Do you want to support this letter? [Sign it here](https://do-not-ship.it/sign/)

## Add a disclaimer to your merge requests

If you want to make it easy for MRs on your project to add a "do-not-ship-WIP" disclaimer
feel free to include our [template](.gitlab/merge_request_templates/wip.md).
